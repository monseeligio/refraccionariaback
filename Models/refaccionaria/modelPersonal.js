const mongoose=require('mongoose');
let Schema=mongoose.Schema;

let nuevoPersonalRefaccionaria = new Schema({
    nombreP:{type:String},
    apellidp:{type:String},
    apellidom:{type:String},
    telefono:{type:String},
    puesto: {type:String},
});

module.exports=mongoose.model('NuevoPersonal', nuevoPersonalRefaccionaria)