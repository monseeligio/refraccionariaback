const mongoose=require('mongoose');
let Schema=mongoose.Schema;

let nuevoSucursalRefaccionaria = new Schema({
    nombreS:{type:String},
    direccion:{type:String},
    encargado:{type:String},
    telefono: {type:String},
    noEmpleados:{type:Number},
});

module.exports=mongoose.model('NuevaSurcursal', nuevoSucursalRefaccionaria)