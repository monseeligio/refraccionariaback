const express=require('express')

const modelPersonalNuevo= require('../../Models/refaccionaria/modelPersonal')

let app=express();

app.post('/personal/nuevo', (req, res)=>{
    let body=req.body;
    console.log(body);
    let newSchemaPersonal=new modelPersonalNuevo({
        nombreP:body.nombreP,
        apellidp:body.apellidp,
        apellidom:body.apellidom,
        telefono:body.telefono,
        puesto: body.puesto,
    });
    newSchemaPersonal
    .save()
    .then(
        (data)=>{
            return res.status(200)
            .json({
                ok:true,
                mensaje:'Datos guardados',
                data
            });
        }
    )
    .catch(
        (err)=>{
        return res.status(500)
        .json({
            ok:false,
            mensaje:'No se guardaron los datos',
            err
        });
    });
});

//consultar
app.get('/obtener/personal', async (req, res) => {
    const respuesta = await modelPersonalNuevo.find();
    res.status(200).json({
        ok: true,
        respuesta
    });
});

// eliminar 
app.delete('/delete/Personal/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelPersonalNuevo.findByIdAndDelete(id);
    res.status(200).json({
        ok: true,
        respuesta,
        msg: "REGISTRO ELIMINADO EXITOSAMENTE"
    });
});

app.put('/update/personal/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;
    const respuesta = await modelPersonalNuevo.findByIdAndUpdate(id, campos, { new: true });
    res.status(202).json({
        ok: true,
        respuesta,
        msg: "ACTUALIZADO EXITOSAMENTE"
    });
});


module.exports=app;
