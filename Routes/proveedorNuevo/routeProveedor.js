const express=require('express')

const modelProveedorNuevo= require('../../Models/refaccionaria/modelProveedores')

let app=express();

app.post('/proveedor/nuevo', (req, res)=>{
    let body=req.body;
    console.log(body);
    let newSchemaProveedor=new modelProveedorNuevo({
        nombrePro:body.nombrePro,
        telefono:body.telefono,
        direccion:body.direccion,
        email: body.email,
        categoria:body.categoria,
    });

    newSchemaProveedor
    .save()
    .then(
        (data)=>{
            return res.status(200)
            .json({
                ok:true,
                mensaje:'Datos guardados',
                data
            });
        }
    )
    .catch(
        (err)=>{
        return res.status(500)
        .json({
            ok:false,
            mensaje:'No se guardaron los datos',
            err
        });
    });
});
app.get('/obtener/proveedor', async (req, res) => {
    const respuesta = await modelProveedorNuevo.find();
    res.status(200).json({
        ok: true,
        respuesta
    });
});

// eliminar 
app.delete('/delete/Proveedor/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelProveedorNuevo.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        respuesta,
        msg: "REGISTRO ELIMINADO EXITOSAMENTE"
    });
});

app.put('/update/proveedor/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;
    const respuesta = await modelProveedorNuevo.findByIdAndUpdate(id, campos, { new: true });
    res.status(202).json({
        ok: true,
        respuesta,
        msg: "ACTUALIZADO EXITOSAMENTE"
    });
});


module.exports=app;
