const express=require('express')

const modelSucursalNuevo= require('../../Models/refaccionaria/modelSucursal')

let app=express();

app.post('/sucursal/nuevo', (req, res)=>{
    let body=req.body;
    console.log(body);
    let newSchemaSucursal=new modelSucursalNuevo({
        nombreS:body.nombreS,
        direccion:body.direccion,
        encargado:body.encargado,
        telefono: body.telefono,
        noEmpleados:body.noEmpleados,
    });
    newSchemaSucursal
    .save()
    .then(
        (data)=>{
            return res.status(200)
            .json({
                ok:true,
                mensaje:'Datos guardados',
                data
            });
        }
    )
    .catch(
        (err)=>{
        return res.status(500)
        .json({
            ok:false,
            mensaje:'No se guardaron los datos',
            err
        });
    });
});

app.get('/obtener/sucursal', async (req, res) => {
    const respuesta = await modelSucursalNuevo.find();
    res.status(200).json({
        ok: true,
        respuesta
    });
});

// eliminar reconocimiento
app.delete('/delete/sucursal/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelSucursalNuevo.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        respuesta,
        msg: "REGISTRO ELIMINADO EXITOSAMENTE"
    });
});

app.put('/update/sucursal/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    //delete campos.nombre
    //delete campos.apellidos
    //delete campos.role

    const respuesta = await modelSucursalNuevo.findByIdAndUpdate(id, campos, { new: true });

    res.status(202).json({
        ok: true,
        respuesta,
        msg: "ACTUALIZADO EXITOSAMENTE"
    });
});



module.exports=app;
