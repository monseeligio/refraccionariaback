const express=require('express');
const app=express();
app.use(require('./test'));

app.use(require('./productoNuevo/routeProductoNuevo'))
app.use(require('./personalNuevo/routePersonal'))
app.use(require('./SucursalNuevo/routeSucursal'))
app.use(require('./proveedorNuevo/routeProveedor'))



module.exports=app;