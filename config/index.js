'use strict'
const mongoose=require('mongoose');
const app=require('../Server/server');
const port=3900;

//generar promesa global
mongoose.Promise=global.Promise;

//hacer la conexion a la bd
mongoose.connect('mongodb://localhost:27017/refaccionaria', 
    {useNewUrlParser:true})
    .then(()=>{
        console.log('Base de datos corriendo');
        //escucha el puerto del servidor
        app.listen(port, ()=>{
            console.log(`server corriendo en el puerto: ${port}`);
        });
    });

